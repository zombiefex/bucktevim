" load and activate pathogen (need to load other plugins in bundle folder)
execute pathogen#infect()
" Main Setup
syntax on
filetype plugin indent on
:set regexpengine=1
" load theme
let g:molokai_original = 1
" Linenumbers
:set relativenumber
:set number
" Automatisches Codeeinrücken
:set autoindent
" Set Tabs
set tabstop=4
set shiftwidth=4
" show Tabs and whitespaces
:set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
:set list
:set syntax=whitespace
"Für P80 Masuchisten
highlight ColorColum ctermbg=magenta
call matchadd('ColorColum', '\%81v', 100)
""Hate white spaces
:autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
"Highlight search
set hlsearch
hi Search guibg=LightBlue
" *** KEYBINDINGS
noremap <F3> :Autoformat<CR>
" use emmet wit Ctrl+y -> , with cursor on Zend
map <C-n> :NERDTreeToggle<CR>
" TabMovement
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . tabpagenr()<CR>
" Angular stuff
let g:used_javascript_libs = 'underscore,backbone,jquery,angularjs,angularui,angularuirouter'
let g:syntastic_html_tidy_ignore_errors=['proprietary attribute "ng-']
let g:syntastic_ts_tsc_ignore_errors=["Cannot find name 'angular"]
