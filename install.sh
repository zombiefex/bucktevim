#!/bin/bash

# setup system
sudo apt-get update && sudo apt-get install curl git vim ncurses-term
sudo npm install coffee-script less typescript -g

# activate 256 colors in terminal
echo "export TERM=xterm-256color" >> ~/.bashrc

# install pathogen
mkdir -p ~/.vim/autoload/ ~/.vim/bundle/ ~/.vim/color
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
curl -LSso ~/.vim/color/molokai.vim https://github.com/tomasr/molokai/blob/master/colors/molokai.vim

# copy vimrc
cp .vimrc ~/.vimrc

# install plugins
cd ~/.vim/bundle
git clone https://github.com/Chiel92/vim-autoformat ~/.vim/bundle/vim-autoformat
git clone https://github.com/bling/vim-airline.git ~/.vim/bundle/vim-airline
git clone https://github.com/mattn/emmet-vim.git ~/.vim/bundle/emmet-vim
git clone git://github.com/jiangmiao/auto-pairs.git ~/.vim/bundle/auto-pairs
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
git clone https://github.com/kchmck/vim-coffee-script.git ~/.vim/bundle/vim-coffee-script/
git clone https://github.com/kien/ctrlp.vim.git ~/.vim/bundle/ctrlp/
git clone https://github.com/leafgarland/typescript-vim.git ~/.vim/bundle/typescript-vim
git clone https://github.com/Quramy/tsuquyomi.git ~/.vim/bundle/tsuquyomi
git clone https://github.com/mklabs/grunt.vim.git ~/.vim/bundle/gruntvim
git clone https://github.com/burnettk/vim-angular.git ~/.vim/bundle/vim-angular
git clone https://github.com/pangloss/vim-javascript.git ~/.vim/bundle/vim-javascript
git clone https://github.com/othree/javascript-libraries-syntax.vim.git ~/.vim/bundle/javascript-libaries-syntax
git clone https://github.com/groenewege/vim-less.git ~/.vim/bundle/vim-less
# fugitive
git clone https://github.com/tpope/vim-fugitive.git ~/.vim/bundle/vim-fugitive
vim -u NONE -c "helptags vim-fugitive/doc" -c q
# typescript (must compile)
git clone https://github.com/Shougo/vimproc.vim.git ~/.vim/bundle/vimproc.vim
cd ~/.vim/bundle/vimproc.vim
make
